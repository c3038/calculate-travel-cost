<?php
declare(strict_types=1);

namespace App\Tests\Helper;

use App\Cost\Domain\Entity\DiscountByAge;
use App\Cost\Domain\Entity\DiscountByPaymentDate;
use App\Cost\Domain\Entity\DiscountByTravelDate;
use App\Cost\Domain\ValueObject\Age;
use App\Cost\Domain\ValueObject\AgeLimit;
use App\Cost\Domain\ValueObject\Amount;
use App\Cost\Domain\ValueObject\DataLimit;
use Codeception\Module;
use Codeception\Module\DataFactory;
use DateInterval;
use League\FactoryMuffin\Faker\Facade;

class Factories extends Module
{
    public function _beforeSuite($settings = []): void
    {
        /** @var DataFactory $factory */
        $factory = $this->getModule('DataFactory');

        $factory->_define(
            DiscountByAge::class,
            [
                'ageLimit' => new AgeLimit(
                    new Age(Facade::numberBetween(0, 20)()),
                    new Age(Facade::numberBetween(20, 100)())
                ),
                'amount' => new Amount(Facade::numberBetween(0, 100)()),
                'max_discount_limit' => null,
                'createdAt' => $this->setDateTimeNow(),
                'updatedAt' => $this->setDateTimeNow(),
            ]
        );

        $factory->_define(
            DiscountByPaymentDate::class,
            [
                'discountByTravelDate' => null,
                'dataLimit' => new DataLimit(
                    $this->setDateTimeNow(),
                    $this->setDateTimeNow()->add(new DateInterval("P1M")),
                ),
                'amount' => new Amount(Facade::numberBetween(0, 100)()),
//                'maxDiscountLimit' => null,
                'createdAt' => $this->setDateTimeNow(),
                'updatedAt' => $this->setDateTimeNow(),
            ]
        );

        $factory->_define(
            DiscountByTravelDate::class,
            [
                'dataLimit' => new DataLimit(
                    $this->setDateTimeNow(),
                    $this->setDateTimeNow()->add(new DateInterval("P1M")),
                ),
//                'paymentDatesWithSales' => null,
                'createdAt' => $this->setDateTimeNow(),
                'updatedAt' => $this->setDateTimeNow(),
            ]
        );
    }


    private function setDateTimeNow(): \DateTimeImmutable
    {
        return \DateTimeImmutable::createFromFormat('U', (string)time());
    }
}