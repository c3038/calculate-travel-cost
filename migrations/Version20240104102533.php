<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240104102533 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE discount_by_age (id BIGSERIAL NOT NULL, max_discount_limit INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, primary_age INT DEFAULT NULL, final_age INT DEFAULT NULL, amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN discount_by_age.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN discount_by_age.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE discount_by_payment_date (id BIGSERIAL NOT NULL, discount_by_travel_date_id BIGINT DEFAULT NULL, max_discount_limit INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8EC45CFCEB55B9AB ON discount_by_payment_date (discount_by_travel_date_id)');
        $this->addSql('COMMENT ON COLUMN discount_by_payment_date.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN discount_by_payment_date.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN discount_by_payment_date.start_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN discount_by_payment_date.end_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE discount_by_travel_date (id BIGSERIAL NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN discount_by_travel_date.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN discount_by_travel_date.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN discount_by_travel_date.start_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN discount_by_travel_date.end_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE discount_by_payment_date ADD CONSTRAINT FK_8EC45CFCEB55B9AB FOREIGN KEY (discount_by_travel_date_id) REFERENCES discount_by_travel_date (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE discount_by_payment_date DROP CONSTRAINT FK_8EC45CFCEB55B9AB');
        $this->addSql('DROP TABLE discount_by_age');
        $this->addSql('DROP TABLE discount_by_payment_date');
        $this->addSql('DROP TABLE discount_by_travel_date');
    }
}
