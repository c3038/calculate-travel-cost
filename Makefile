help: ## Помощь
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

up: ## Старт контейнера
	docker compose up -d
down: ## Остановка контейнера
	docker compose down

build: ## Сборка проекта
	#cp .env.example .env
	docker compose build
	docker compose up -d
	docker compose exec php-fpm composer install
	#docker compose exec php-fpm bin/console doctrine:migrations:migrate

up-bd:
	docker compose up -d postgres

bash:
	docker compose exec -i php-fpm bash

#./bin/console doctrine:schema:validate
#./bin/console doctrine:migrations:diff
#./bin/console doctrine:migrations:migrate
#./bin/console d:d:c --env=test // создание bd
#./bin/console d:m:m --env=test // миграция
#./vendor/bin/codecept run unit
#./vendor/bin/codecept run functional
#./vendor/bin/codecept run acceptance
#vendor/bin/codecept run clean
#./vendor/bin/codecept build // пересборка тестера
#./bin/console app_init:fill-database // заполнение базы discount_by_age
#./bin/console app_init:fill-travel-payment // заполнение баз discount_by_payment_date, discount_by_travel_date