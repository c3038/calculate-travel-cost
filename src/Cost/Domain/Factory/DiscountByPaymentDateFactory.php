<?php

declare(strict_types=1);

namespace App\Cost\Domain\Factory;

use App\Cost\Domain\Dto\CreateDiscountByPaymentDateDto;
use App\Cost\Domain\Entity\DiscountByPaymentDate;
use App\Cost\Domain\ValueObject\Amount;
use App\Cost\Domain\ValueObject\DataLimit;

class DiscountByPaymentDateFactory
{
    public function create(CreateDiscountByPaymentDateDto $dto): DiscountByPaymentDate
    {
        return (new DiscountByPaymentDate())
            ->setDataLimit(new DataLimit($dto->getStartDate(), $dto->getEndDate()))
            ->setAmount(new Amount($dto->getAmount()))
            ->setMaxDiscountLimit($dto->getMaxDiscountLimit());
    }
}