<?php
declare(strict_types=1);

namespace App\Cost\Domain\Factory;

use App\Cost\Domain\Dto\CreateDiscountByAgeDto;
use App\Cost\Domain\Entity\DiscountByAge;
use App\Cost\Domain\ValueObject\Age;
use App\Cost\Domain\ValueObject\AgeLimit;
use App\Cost\Domain\ValueObject\Amount;

class DiscountByAgeFactory
{
    public function create(CreateDiscountByAgeDto $dto): DiscountByAge
    {
        $primaryAge= new Age($dto->getPrimaryAge());
        $finalAge= new Age($dto->getFinalAge());
        return (new DiscountByAge())
            ->setAgeLimit(new AgeLimit($primaryAge, $finalAge))
            ->setAmount(new Amount($dto->getAmount()))
            ->setMaxDiscountLimit($dto->getMaxDiscountLimit());
    }
}