<?php

declare(strict_types=1);

namespace App\Cost\Domain\Factory;

use App\Cost\Domain\Dto\CreateDiscountByTravelDateDto;
use App\Cost\Domain\Entity\DiscountByTravelDate;
use App\Cost\Domain\ValueObject\DataLimit;

class DiscountByTravelDateFactory
{
    public function create(CreateDiscountByTravelDateDto $dto): DiscountByTravelDate
    {
        return (new DiscountByTravelDate())
            ->setDataLimit(new DataLimit($dto->getStartDate(), $dto->getEndDate()));
    }
}