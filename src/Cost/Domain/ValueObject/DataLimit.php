<?php

declare(strict_types=1);

namespace App\Cost\Domain\ValueObject;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class DataLimit
{
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $startDate;
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $endDate;

    public function __construct(?DateTimeImmutable $startDate, ?DateTimeImmutable $endDate)
    {
        $this->assertAllNull($startDate, $endDate);
        $this->assertOrderLimit($startDate, $endDate);
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    private function assertAllNull(?DateTimeImmutable $startDate, ?DateTimeImmutable $endDate): void
    {
        if ($startDate === null && $endDate === null)
            throw new \InvalidArgumentException('Both dates cannot be null');
    }

    private function assertOrderLimit(?DateTimeImmutable $startDate, ?DateTimeImmutable $endDate): void
    {
        if ($startDate !== null && $endDate !== null && $startDate > $endDate)
            throw new \InvalidArgumentException('Dates of limits is invalid');
    }

    public function getStartDate(): ?DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getEndDate(): ?DateTimeImmutable
    {
        return $this->endDate;
    }

    public function __toString(): string
    {
        return  sprintf('from %s to %s',
            $this->getStartDate()->format('d-m-Y h:i:s'),
            $this->getEndDate()->format('d-m-Y h:i:s')
        );
    }
}