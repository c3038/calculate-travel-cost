<?php

declare(strict_types=1);

namespace App\Cost\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Embeddable]
class Amount
{
    #[Assert\Type(type: 'integer')]
    #[ORM\Column(type: 'integer', nullable: false)]
    private int $amount;

    public function __construct(int $amount)
    {
        $this->assertBabyAgeIsValid($amount);
        $this->amount = $amount;
    }

    private function assertBabyAgeIsValid(int $amount): void
    {
        if (0 > $amount)
            throw new \InvalidArgumentException('Invalid amount');
    }

    public function getValue(): int
    {
        return $this->amount;
    }

    public function __toString(): string
    {
        return (string)$this->amount;
    }
}