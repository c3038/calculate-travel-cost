<?php

declare(strict_types=1);

namespace App\Cost\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class Age
{
    #[ORM\Column(type: 'integer', nullable: true)]
    private int $age;
    public function __construct(int $age)
    {
        $this->assertBabyAgeIsValid($age);
        $this->age = $age;
    }

    private function assertBabyAgeIsValid(int $age): void
    {
        if (0 > $age)
            throw new \InvalidArgumentException('Invalid baby age');
    }

    public function getValue(): int
    {
        return $this->age;
    }

    public function __toString(): string
    {
        return (string) $this->age;
    }
}