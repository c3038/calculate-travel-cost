<?php

declare(strict_types=1);

namespace App\Cost\Domain\ValueObject;

use DateTimeImmutable;

class DataWithFormat
{
    const DEFAULT_INPUT_FORMAT = 'd.m.Y';
    const DEFAULT_OUTPUT_FORMAT = 'Y-m-d h:i:s';
    private ?\DateTimeImmutable $date = null;
    private string $inputFormat;
    private string $outputFormat;

    public function __construct()
    {
        $this->inputFormat = self::DEFAULT_INPUT_FORMAT;
        $this->outputFormat = self::DEFAULT_OUTPUT_FORMAT;
    }

    public function setDate(?string $date): DataWithFormat
    {
        $this->date = $this->setValue($date);

        return $this;
    }


    private function setValue(?string $date): ?\DateTimeImmutable
    {
        if ($date === null) {
            return null;
        }

        return $this->assertIsDate($date);
    }

    private function assertIsDate(string $date): \DateTimeImmutable
    {
        $res = \DateTimeImmutable::createFromFormat($this->getInputFormat(), $date);
        if (!$res) {
            throw new \InvalidArgumentException('Is not date');
        }

        return $res;
    }

    public function getDate(): ?DateTimeImmutable
    {
        return $this->date;
    }

    public function getInputFormat(): string
    {
        return $this->inputFormat;
    }

    public function setInputFormat(string $inputFormat): static
    {
        $this->inputFormat = $inputFormat;
        return $this;
    }

    public function getOutputFormat(): string
    {
        return $this->outputFormat;
    }

    public function setOutputFormat(string $outputFormat): static
    {
        $this->outputFormat = $outputFormat;

        return $this;
    }

    public function getDateTimeNow(): string
    {
        return \DateTimeImmutable::createFromFormat('U', (string)time())->format($this->getOutputFormat());
    }


    public function __toString(): string
    {
        return $this->getDate()?->format($this->getOutputFormat()) ?? '';
    }
}