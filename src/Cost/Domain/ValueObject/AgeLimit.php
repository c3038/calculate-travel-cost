<?php

declare(strict_types=1);

namespace App\Cost\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class AgeLimit
{
    #[ORM\Embedded(class: Age::class)]
    private ?Age $primary;
    #[ORM\Embedded(class: Age::class)]
    private ?Age $final;

    public function __construct(?Age $primary, ?Age $final)
    {
        $this->assertAllNull($primary, $final);
        $this->assertOrderLimit($primary, $final);
        $this->primary = $primary;
        $this->final = $final;
    }

    private function assertAllNull(?Age $primary, ?Age $final): void
    {
        if ($primary->getValue() === null && $final->getValue() === null)
            throw new \InvalidArgumentException('Age limit not be null');
    }

    private function assertOrderLimit(?Age $primary, ?Age $final): void
    {
        if (
            $primary->getValue() !== null
            && $final->getValue() !== null
            && $primary->getValue() > $final->getValue()
        )
            throw new \InvalidArgumentException('Order of limits is invalid');
    }

    public function getPrimary(): ?Age
    {
        return $this->primary;
    }

    public function getFinal(): ?Age
    {
        return $this->final;
    }

    public function __toString(): string
    {
        return  sprintf('from %s to %s',
            $this->getprimary()->getValue(),
            $this->getfinal()->getValue()
        );
    }
}