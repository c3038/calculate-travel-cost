<?php

declare(strict_types=1);

namespace App\Cost\Domain\Dto;

class CreateDiscountByTravelDateDto
{
    public function __construct(
        readonly private \DateTimeImmutable $startDate,
        readonly private ?\DateTimeImmutable $endDate,
    )
    {
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

}