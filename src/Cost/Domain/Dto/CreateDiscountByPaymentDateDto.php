<?php

declare(strict_types=1);

namespace App\Cost\Domain\Dto;

class CreateDiscountByPaymentDateDto
{
    public function __construct(
        readonly private ?\DateTimeImmutable $startDate,
        readonly private \DateTimeImmutable $endDate,
        readonly private int $amount,
        readonly private ?int $maxDiscountLimit
    )
    {
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getEndDate(): \DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getMaxDiscountLimit(): ?int
    {
        return $this->maxDiscountLimit;
    }


}