<?php
declare(strict_types=1);

namespace App\Cost\Domain\Dto;

class CreateDiscountByAgeDto
{
    public function __construct(
        readonly private int $primaryAge,
        readonly private int $finalAge,
        readonly private int $amount,
        readonly private ?int $maxDiscountLimit
    )
    {
    }

    public function getPrimaryAge(): int
    {
        return $this->primaryAge;
    }

    public function getFinalAge(): int
    {
        return $this->finalAge;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getMaxDiscountLimit(): ?int
    {
        return $this->maxDiscountLimit;
    }

}