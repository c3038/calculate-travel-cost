<?php
declare(strict_types=1);

namespace App\Cost\Domain\Repository;

use App\Cost\Domain\Entity\DiscountByPaymentDate;

interface DiscountByPaymentDateRepositoryInterface
{
    public function addWithoutFlush(DiscountByPaymentDate $discountByPaymentDate): void;
}