<?php

declare(strict_types=1);

namespace App\Cost\Domain\Repository;

use App\Cost\Application\Dto\DiscountFindResultDto;
use App\Cost\Domain\Entity\DiscountByTravelDate;

interface DiscountByTravelDateInterface
{
    public function add(DiscountByTravelDate $discountByTravelDate): void;

    public function addWithoutFlush(DiscountByTravelDate $discountByTravelDate): void;

    public function flush(): void;

    public function findById(int $id): ?DiscountByTravelDate;

    public function findByTravelDate(\DateTimeImmutable $travelDate): ?array;

    public function findDiscountByDate(\DateTimeImmutable $travelDate, \DateTimeImmutable $payDate): DiscountFindResultDto;

    public function deleteItem(DiscountByTravelDate $discountByTravelDate): void;

    public function removeWithoutFlush(DiscountByTravelDate $discountByTravelDate): void;
}