<?php
declare(strict_types=1);

namespace App\Cost\Domain\Repository;

use App\Cost\Application\Dto\DiscountFindResultDto;
use App\Cost\Domain\Entity\DiscountByAge;

interface DiscountByAgeRepositoryInterface
{
    public function add(DiscountByAge $discountByAge): void;

    public function addWithoutFlush(DiscountByAge $discountByAge): void;

    public function flush(): void;

    public function findById(int $id): ?DiscountByAge;

    public function findByAge(int $searchAge): ?DiscountFindResultDto;

    public function deleteItem(DiscountByAge $discountByAge): void;

    public function removeWithoutFlush(DiscountByAge $discountByAge): void;
}