<?php

declare(strict_types=1);

namespace App\Cost\Domain\Entity;

use App\Cost\Domain\ValueObject\AgeLimit;
use App\Cost\Domain\ValueObject\Amount;
use App\Cost\Infrastructure\Repository\DiscountByAgeRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: DiscountByAgeRepository::class)]
#[ORM\Table(name: 'discount_by_age')]
class DiscountByAge
{
    #[ORM\Column(name: 'id', type: Types::BIGINT, unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Embedded(class: AgeLimit::class, columnPrefix: false)]
    private AgeLimit $ageLimit;

    #[ORM\Embedded(class: Amount::class, columnPrefix: false)]
    private Amount $amount;
    #[ORM\Column(name: 'max_discount_limit', type: 'integer', nullable: true)]
    private ?int $maxDiscountLimit;

    #[ORM\Column(name: 'created_at', type: 'datetime_immutable', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime_immutable', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private \DateTimeImmutable $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getAgeLimit(): AgeLimit
    {
        return $this->ageLimit;
    }

    public function setAgeLimit(AgeLimit $ageLimit): static
    {
        $this->ageLimit = $ageLimit;
        return $this;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function setAmount(Amount $amount): static
    {
        $this->amount = $amount;
        return $this;
    }

    public function getMaxDiscountLimit(): ?int
    {
        return $this->maxDiscountLimit;
    }

    public function setMaxDiscountLimit(?int $maxDiscountLimit): static
    {
        $this->maxDiscountLimit = $maxDiscountLimit;
        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(): static
    {
        $this->createdAt = \DateTimeImmutable::createFromFormat('U',(string)time());
        return $this;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): static
    {
        $this->updatedAt = \DateTimeImmutable::createFromFormat('U',(string)time());
        return $this;
    }
}