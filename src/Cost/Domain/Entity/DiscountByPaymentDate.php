<?php

declare(strict_types=1);

namespace App\Cost\Domain\Entity;

use App\Cost\Domain\ValueObject\Amount;
use App\Cost\Domain\ValueObject\DataLimit;
use App\Cost\Infrastructure\Repository\DiscountByPaymentDateRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: DiscountByPaymentDateRepository::class)]
#[ORM\Table(name: 'discount_by_payment_date')]
class DiscountByPaymentDate
{
    #[ORM\Column(name: 'id', type: Types::BIGINT, unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: 'DiscountByTravelDate', cascade: ['persist'], inversedBy: 'paymentDatesWithSales')]
    #[ORM\JoinColumn(name: 'discount_by_travel_date_id', referencedColumnName: 'id')]
    private DiscountByTravelDate $discountByTravelDate;

    #[ORM\Embedded(class: DataLimit::class, columnPrefix: false)]
    private DataLimit $dataLimit;

    #[ORM\Embedded(class: Amount::class, columnPrefix: false)]
    private Amount $amount;

    #[ORM\Column(name: 'max_discount_limit', type: 'integer', nullable: true)]
    private int $maxDiscountLimit;

    #[ORM\Column(name: 'created_at', type: 'datetime_immutable', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime_immutable', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private \DateTimeImmutable $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getDiscountByTravelDate(): DiscountByTravelDate
    {
        return $this->discountByTravelDate;
    }

    public function setDiscountByTravelDate(DiscountByTravelDate $discountByTravelDate): static
    {
        $this->discountByTravelDate = $discountByTravelDate;
        return $this;
    }
    public function getDataLimit(): DataLimit
    {
        return $this->dataLimit;
    }

    public function setDataLimit(DataLimit $dataLimit): static
    {
        $this->dataLimit = $dataLimit;
        return $this;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function setAmount(Amount $amount): static
    {
        $this->amount = $amount;
        return $this;
    }

    public function getMaxDiscountLimit(): int
    {
        return $this->maxDiscountLimit;
    }

    public function setMaxDiscountLimit(int $maxDiscountLimit): static
    {
        $this->maxDiscountLimit = $maxDiscountLimit;
        return $this;
    }


    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}