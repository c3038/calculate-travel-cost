<?php
declare(strict_types=1);

namespace App\Cost\Domain\Entity;

use App\Cost\Domain\ValueObject\DataLimit;
use App\Cost\Infrastructure\Repository\DiscountByTravelDateRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: DiscountByTravelDateRepository::class)]
#[ORM\Table(name: 'discount_by_travel_date')]
class DiscountByTravelDate
{
    #[ORM\Column(name: 'id', type: Types::BIGINT, unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Embedded(class: DataLimit::class, columnPrefix: false)]
    private DataLimit $dataLimit;

    #[ORM\OneToMany(mappedBy: 'discountByTravelDate', targetEntity: 'DiscountByPaymentDate', cascade: ['persist'])]
    private Collection $paymentDatesWithSales;

    #[ORM\Column(name: 'created_at', type: 'datetime_immutable', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime_immutable', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private \DateTimeImmutable $updatedAt;

    public function __construct()
    {
        $this->paymentDatesWithSales = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getDataLimit(): DataLimit
    {
        return $this->dataLimit;
    }

    public function setDataLimit(DataLimit $dataLimit): static
    {
        $this->dataLimit = $dataLimit;
        return $this;
    }

    public function getPaymentDatesWithSales(): Collection
    {
        return $this->paymentDatesWithSales;
    }


    public function setPaymentDatesWithSales(DiscountByPaymentDate $paymentDatesWithSales): static
    {
        if (!$this->paymentDatesWithSales->contains($paymentDatesWithSales)) {
            $this->paymentDatesWithSales->add($paymentDatesWithSales);
        }
        return $this;
    }


    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(): static
    {
        $this->createdAt = \DateTimeImmutable::createFromFormat('U',(string)time());
        return $this;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): static
    {
        $this->updatedAt = \DateTimeImmutable::createFromFormat('U',(string)time());
        return $this;
    }
}