<?php
declare(strict_types=1);

namespace App\Cost\Infrastructure\Repository;

use App\Cost\Domain\Entity\DiscountByPaymentDate;
use App\Cost\Domain\Repository\DiscountByPaymentDateRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DiscountByPaymentDateRepository extends ServiceEntityRepository implements DiscountByPaymentDateRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountByPaymentDate::class);
    }

    public function addWithoutFlush(DiscountByPaymentDate $discountByPaymentDate): void
    {
        $this->_em->persist($discountByPaymentDate);
    }
}