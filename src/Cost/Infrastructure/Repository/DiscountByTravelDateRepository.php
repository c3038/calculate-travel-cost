<?php
declare(strict_types=1);

namespace App\Cost\Infrastructure\Repository;

use App\Cost\Application\Dto\DiscountFindNullResultDto;
use App\Cost\Application\Dto\DiscountFindResultDto;
use App\Cost\Domain\Entity\DiscountByTravelDate;
use App\Cost\Domain\Repository\DiscountByTravelDateInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DiscountByTravelDateRepository extends ServiceEntityRepository implements DiscountByTravelDateInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountByTravelDate::class);
    }

    public function add(DiscountByTravelDate $discountByTravelDate): void
    {
        $this->_em->persist($discountByTravelDate);
        $this->_em->flush();
    }

    public function addWithoutFlush(DiscountByTravelDate $discountByTravelDate): void
    {
        $this->_em->persist($discountByTravelDate);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    public function findById(int $id): ?DiscountByTravelDate
    {
        return $this->find($id);
    }

    public function findByTravelDate(\DateTimeImmutable $travelDate): ?array
    {
        $q = $this->_em->createQuery(
            'SELECT dtd FROM App\Cost\Domain\Entity\DiscountByTravelDate dtd
             WHERE dtd.dataLimit.startDate <= :travelDate
             AND (dtd.dataLimit.endDate IS NULL OR dtd.dataLimit.endDate >= :yourDate)'
        );

        $q->setParameter('yourDate', $travelDate);

        return $q->getResult();
    }

    public function findDiscountByDate(\DateTimeImmutable $travelDate, \DateTimeImmutable $payDate): DiscountFindResultDto
    {
        $query = $this->_em->createQuery(
            'SELECT pd.amount.amount, pd.maxDiscountLimit
            FROM App\Cost\Domain\Entity\DiscountByTravelDate t
             LEFT JOIN App\Cost\Domain\Entity\DiscountByPaymentDate pd WITH t.id = pd.discountByTravelDate
             WHERE
             t.dataLimit.startDate <= :desiredTravelDate
             AND (t.dataLimit.endDate IS NULL OR t.dataLimit.endDate >= :desiredTravelDate)
             AND (pd.dataLimit.startDate IS NULL OR pd.dataLimit.startDate <= :desiredPurchaseDate)
             AND pd.dataLimit.endDate >= :desiredPurchaseDate'
        );

        $query->setParameter('desiredTravelDate', $travelDate);
        $query->setParameter('desiredPurchaseDate', $payDate);

        $res = $query->getOneOrNullResult();

        if(!isset($res)) {
            return new DiscountFindNullResultDto();
        }

        return new DiscountFindResultDto($res['amount.amount'], $res['maxDiscountLimit']);
    }

    public function deleteItem(DiscountByTravelDate $discountByTravelDate): void
    {
        $this->_em->remove($discountByTravelDate);
        $this->_em->flush();
    }

    public function removeWithoutFlush(DiscountByTravelDate $discountByTravelDate): void
    {
        $this->_em->remove($discountByTravelDate);
    }
}