<?php

declare(strict_types=1);

namespace App\Cost\Infrastructure\Repository;

use App\Cost\Application\Dto\DiscountFindResultDto;
use App\Cost\Domain\Entity\DiscountByAge;
use App\Cost\Domain\Repository\DiscountByAgeRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DiscountByAgeRepository extends ServiceEntityRepository implements DiscountByAgeRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountByAge::class);
    }

    public function add(DiscountByAge $discountByAge): void
    {
        $this->_em->persist($discountByAge);
        $this->_em->flush();
    }

    public function addWithoutFlush(DiscountByAge $discountByAge): void
    {
        $this->_em->persist($discountByAge);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    public function findById(int $id): ?DiscountByAge
    {
        return $this->find($id);
    }

    public function findByAge(int $searchAge): ?DiscountFindResultDto
    {
        $qb = $this->createQueryBuilder('d')
            ->select('d.amount.amount', 'd.maxDiscountLimit')
            ->where(':searchAge BETWEEN d.ageLimit.primary.age AND d.ageLimit.final.age')
            ->setParameter('searchAge', $searchAge);

        $res = $qb->getQuery()->getOneOrNullResult();

        return new DiscountFindResultDto($res['amount.amount'], $res['maxDiscountLimit']);
    }

    public function deleteItem(DiscountByAge $discountByAge): void
    {
        $this->_em->remove($discountByAge);
        $this->_em->flush();
    }

    public function removeWithoutFlush(DiscountByAge $discountByAge): void
    {
        $this->_em->remove($discountByAge);
    }
}