<?php

declare(strict_types=1);

namespace App\Cost\Infrastructure\Command;

use App\Cost\Application\Manager\DiscountByAgeManager;
use App\Cost\Domain\Dto\CreateDiscountByAgeDto;
use App\Cost\Domain\Factory\DiscountByAgeFactory;
use App\Cost\Domain\Repository\DiscountByAgeRepositoryInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: self::INITIAL_BD_COMMAND_NAME,
    description: 'Init database discount_by_age for new start application',
)]
final class InitDataInDBCommand extends Command
{
    public const INITIAL_BD_COMMAND_NAME = 'app_init:fill-database';

    public function __construct(
        private readonly  DiscountByAgeRepositoryInterface $discountByAgeRepository,
        private readonly DiscountByAgeFactory $discountByAgeFactory,
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $initialData = [
            [
                'primaryAge' => 3,
                'finalAge' => 5,
                'amount' => 80,
                'maxDiscountLimit' => null,
            ],
            [
                'primaryAge' => 6,
                'finalAge' => 11,
                'amount' => 30,
                'maxDiscountLimit' => 4500,
            ],
            [
                'primaryAge' => 12,
                'finalAge' => 13,
                'amount' => 10,
                'maxDiscountLimit' => null,
            ],
        ];

        foreach ($initialData as $initData) {
            $data = new CreateDiscountByAgeDto(
                $initData['primaryAge'],
                $initData['finalAge'],
                $initData['amount'],
                $initData['maxDiscountLimit'],
            );
            $discountByAge = $this->discountByAgeFactory->create($data);
            $this->discountByAgeRepository->addWithoutFlush($discountByAge);
        }
        $this->discountByAgeRepository->flush();

        $output->writeln('Database successfully filled.');

        return self::SUCCESS;
    }
}