<?php

declare(strict_types=1);

namespace App\Cost\Infrastructure\Command;

use App\Cost\Application\Utils\MocDataGenerator;
use App\Cost\Domain\Dto\CreateDiscountByPaymentDateDto;
use App\Cost\Domain\Dto\CreateDiscountByTravelDateDto;
use App\Cost\Domain\Factory\DiscountByPaymentDateFactory;
use App\Cost\Domain\Factory\DiscountByTravelDateFactory;
use App\Cost\Domain\Repository\DiscountByPaymentDateRepositoryInterface;
use App\Cost\Domain\Repository\DiscountByTravelDateInterface;
use App\Cost\Domain\ValueObject\DataWithFormat;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: self::INITIAL_BD_COMMAND_NAME,
    description: 'Init database discount_by_age for new start application',
)]
final class InitDataInDBTravelPaymentCommand extends Command
{
    public const INITIAL_BD_COMMAND_NAME = 'app_init:fill-travel-payment';
    private array $initialTravelData = [];
    private array $initialPaymentData = [];

    public function __construct(
        private readonly DiscountByTravelDateInterface            $discountByTravelDateRepository,
        private readonly DiscountByTravelDateFactory              $travelDateFactory,
        private readonly DiscountByPaymentDateRepositoryInterface $discountByPaymentDateRepository,
        private readonly DiscountByPaymentDateFactory             $paymentDateFactory,
        private readonly MocDataGenerator                         $dataGenerator,
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->initialTravelData = $this->dataGenerator->getInitialTravelData();
        $this->initialPaymentData = $this->dataGenerator->getInitialPaymentData();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->initialTravelData as $key => $travelData) {
            $data = new CreateDiscountByTravelDateDto(
                (new DataWithFormat())->setDate($travelData['startDate'])->getDate(),
                (new DataWithFormat())->setDate($travelData['endDate'])->getDate(),
            );
            $discountTravelDate = $this->travelDateFactory->create($data);
            foreach ($this->initialPaymentData[$key] as $paymentData) {
                $discountByPaymentDto = new CreateDiscountByPaymentDateDto(
                    (new DataWithFormat())->setDate($paymentData['startDate'])->getDate(),
                    (new DataWithFormat())->setDate($paymentData['endDate'])->getDate(),
                    $paymentData['amount'],
                    $paymentData['maxDiscountLimit'],
                );
                $discountByPayment = $this->paymentDateFactory->create($discountByPaymentDto);
                $discountByPayment->setDiscountByTravelDate($discountTravelDate);
                $this->discountByPaymentDateRepository->addWithoutFlush($discountByPayment);
            }
            $this->discountByTravelDateRepository->addWithoutFlush($discountTravelDate);
        }
        $this->discountByTravelDateRepository->flush();

        $output->writeln('Database successfully filled.');

        return self::SUCCESS;
    }
}