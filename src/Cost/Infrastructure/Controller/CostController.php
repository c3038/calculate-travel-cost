<?php

declare(strict_types=1);

namespace App\Cost\Infrastructure\Controller;

use App\Cost\Application\Dto\DiscountPriceCalculationDto;
use App\Cost\Application\Dto\PriceResultDto;
use App\Cost\Application\Manager\CostManager;
use App\Cost\Infrastructure\Controller\Output\HttpErrorMessage;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationListInterface;

#[Route(path: 'api/v1/costs')]
class CostController extends AbstractFOSRestController
{
    public function __construct(
        readonly private CostManager $costManager
    )
    {
    }

    #[OA\RequestBody(
        required: true,
        content: new OA\JsonContent(ref: new Model(type: DiscountPriceCalculationDto::class))
    )]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return price',
        content: new OA\JsonContent(
            allOf: [
                new OA\Schema(ref: new Model(type: PriceResultDto::class))
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_BAD_REQUEST,
        description: 'Bad request',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 400
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Стоимость путешествия обязательна!'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Cost')]
    #[Route('', name: 'costWithSailCalculate', methods: ['POST'])]
    #[ParamConverter('dto', class: DiscountPriceCalculationDto::class, converter: 'fos_rest.request_body')]
    public function costWithSailCalculate(
        DiscountPriceCalculationDto      $dto,
        ConstraintViolationListInterface $validationErrors
    ): Response
    {
        if (count($validationErrors) > 0) {
            $view = $this->view(
                new HttpErrorMessage(Response::HTTP_BAD_REQUEST, $validationErrors->get(0)->getMessage()),
                Response::HTTP_BAD_REQUEST
            );
            return $this->handleView($view);
        }
        $cost = $this->costManager->calculatePrice($dto);
        $view = $this->view($cost, Response::HTTP_OK);

        return $this->handleView($view);
    }
}