<?php

declare(strict_types=1);

namespace App\Cost\Infrastructure\Controller\Output;

class HttpErrorMessage
{
    public function __construct(
        public int $code, public string $message)
    {
    }
}
