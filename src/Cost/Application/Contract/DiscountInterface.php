<?php
declare(strict_types=1);

namespace App\Cost\Application\Contract;

interface DiscountInterface
{
    public function getAmountDiscount():int;
}