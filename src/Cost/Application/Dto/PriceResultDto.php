<?php
declare(strict_types=1);

namespace App\Cost\Application\Dto;

use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'PriceResultDto',
    title: 'Объект отображения результата расчета стоимости путешествия',
    properties: [
        new OA\Property(
            property: 'price',
            type: 'integer',
            example: '10000'
        ),
    ]
)]
class PriceResultDto
{
    public function __construct(
        readonly private int $price,
    )
    {
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}