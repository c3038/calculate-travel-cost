<?php
declare(strict_types=1);

namespace App\Cost\Application\Dto;

class DiscountFindResultDto
{
    const NOT_DISCOUNT_LIMIT = 0;
    public function __construct(
        readonly private int $amount,
        readonly private ?int $maxDiscountLimit
    )
    {
    }

    public function isNull():bool
    {
        return false;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getMaxDiscountLimit(): int
    {
        return $this->maxDiscountLimit ?? static::NOT_DISCOUNT_LIMIT;
    }


}