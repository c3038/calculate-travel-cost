<?php
declare(strict_types=1);

namespace App\Cost\Application\Dto;

class DiscountByTravelDateCreatedDto
{
    public function __construct(
        readonly private ?int                $id,
        readonly private ?\DateTimeImmutable $startDate,
        readonly private ?\DateTimeImmutable $endDate,
        readonly private \DateTimeImmutable  $createdAt,
        readonly private \DateTimeImmutable  $updatedAt,
    )
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?string
    {
        return $this->startDate->format('Y-m-d h:i:s');
    }

    public function getEndDate(): ?string
    {
        return $this->endDate->format('Y-m-d h:i:s');
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt->format('Y-m-d h:i:s');
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format('Y-m-d h:i:s');
    }
}