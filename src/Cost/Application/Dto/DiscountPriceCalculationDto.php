<?php
declare(strict_types=1);

namespace App\Cost\Application\Dto;

use DateTimeImmutable;
use JMS\Serializer\Annotation as JMS;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[OA\Schema(
    schema: 'DiscountPriceCalculationDto',
    title: 'Объект запроса для расчета стоимости путешествия',
    properties: [
        new OA\Property(
            property: 'price',
            type: 'integer',
            example: '10000'
        ),
        new OA\Property(
            property: 'date_of_birth',
            type: 'string',
            format: 'd.m.Y',
            example: '01.01.2020'
        ),
        new OA\Property(
            property: 'travel_start_date',
            type: 'string',
            format: 'd.m.Y',
            example: '01.05.2024'
        ),
        new OA\Property(
            property: 'date_of_payment',
            type: 'string',
            format: 'd.m.Y',
            example: '10.01.2024'
        ),
    ]
)]
class DiscountPriceCalculationDto
{
    #[Assert\NotBlank(message: 'Стоимость путешествия обязательна!')]
//    #[Assert\Type(type: 'integer', message:'Должно быть числом')]
//    #[Assert\Valid]
    private int $price;
    #[Assert\NotBlank(message: 'Дата рождения обязательна!')]
    #[JMS\Type("DateTimeImmutable<'d.m.Y'>")]
    private DateTimeImmutable $dateOfBirth;
    #[JMS\Type("DateTimeImmutable<'d.m.Y'>")]
    private ?DateTimeImmutable $travelStartDate;
    #[JMS\Type("DateTimeImmutable<'d.m.Y'>")]
    private ?DateTimeImmutable $dateOfPayment;

    public function __construct(
        int                $price,
        DateTimeImmutable  $dateOfBirth,
        ?DateTimeImmutable $travelStartDate = null,
        ?DateTimeImmutable $dateOfPayment = null,
    )
    {
        $this->price = $price;
        $this->dateOfBirth = $dateOfBirth;
        $this->travelStartDate = $travelStartDate ?? new DateTimeImmutable;
        $this->dateOfPayment = $dateOfPayment ?? new DateTimeImmutable;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getDateOfBirth(): DateTimeImmutable
    {
        return $this->dateOfBirth;
    }

    public function getTravelStartDate(): ?DateTimeImmutable
    {
        return $this->travelStartDate;
    }

    public function getDateOfPayment(): ?DateTimeImmutable
    {
        return $this->dateOfPayment;
    }
}