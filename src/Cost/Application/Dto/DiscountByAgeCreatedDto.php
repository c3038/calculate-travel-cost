<?php
declare(strict_types=1);

namespace App\Cost\Application\Dto;

class DiscountByAgeCreatedDto
{
    public function __construct(
        readonly private  ?int $id,
        readonly private ?int $primaryAge,
        readonly private ?int $finalAge,
        readonly private int $amount,
        readonly private ?int $maxDiscountLimit,
        readonly private \DateTimeImmutable $createdAt,
        readonly private \DateTimeImmutable $updatedAt,
    )
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrimaryAge(): ?int
    {
        return $this->primaryAge;
    }

    public function getFinalAge(): ?int
    {
        return $this->finalAge;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getMaxDiscountLimit(): ?int
    {
        return $this->maxDiscountLimit;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt->format('Y-m-d h:i:s');
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format('Y-m-d h:i:s');
    }
}