<?php
declare(strict_types=1);

namespace App\Cost\Application\Dto;

class DiscountFindNullResultDto extends DiscountFindResultDto
{
    const NULL_AMOUNT = 0;
    const NULL_MAX_DISCOUNT_LIMIT = null;
    public function __construct(
    )
    {
        parent::__construct(static::NULL_AMOUNT, static::NULL_MAX_DISCOUNT_LIMIT);
    }

    public function isNull():bool
    {
        return true;
    }
}