<?php

declare(strict_types=1);

namespace App\Cost\Application\Manager;

use App\Cost\Application\Dto\DiscountByAgeCreatedDto;
use App\Cost\Application\Mapper\DiscountByAgeMapper;
use App\Cost\Domain\Dto\CreateDiscountByAgeDto;
use App\Cost\Domain\Factory\DiscountByAgeFactory;
use App\Cost\Infrastructure\Repository\DiscountByAgeRepository;

class DiscountByAgeManager
{
    public function __construct(
        private readonly DiscountByAgeMapper     $discountByAgeMapper,
        private readonly DiscountByAgeRepository $discountByAgeRepository,
        private readonly DiscountByAgeFactory    $discountByAgeFactory,
    )
    {
    }

    public function create(CreateDiscountByAgeDto $dto): DiscountByAgeCreatedDto
    {
        $discountByAge = $this->discountByAgeFactory->create($dto);
        $this->discountByAgeRepository->add($discountByAge);

        return $this->discountByAgeMapper->toCreatedDto($discountByAge);
    }
}