<?php

declare(strict_types=1);

namespace App\Cost\Application\Manager;

use App\Cost\Application\Dto\DiscountPriceCalculationDto;
use App\Cost\Application\Dto\PriceResultDto;
use App\Cost\Application\Manager\Discount\AgeDiscount;
use App\Cost\Application\Manager\Discount\PriceCalculator;
use App\Cost\Application\Manager\Discount\PriceDiscount;


class CostManager
{
    public function __construct(
        private readonly PriceCalculator $priceCalculator,
        private AgeDiscount              $ageDiscount,
        private PriceDiscount            $priceDiscount,
    )
    {
    }

    public function calculatePrice(DiscountPriceCalculationDto $dto): PriceResultDto
    {
        $price =  $this->priceCalculator
            ->setDiscount(
                $this->ageDiscount->to($dto->getDateOfBirth(), $dto->getPrice())
            )
            ->setDiscount(
                $this->priceDiscount->to($dto->getTravelStartDate(), $dto->getDateOfPayment(), $dto->getPrice())
            )
            ->calculate($dto->getPrice());

        return  new PriceResultDto($price);
    }


}