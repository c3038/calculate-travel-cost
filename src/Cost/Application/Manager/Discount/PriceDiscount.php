<?php
declare(strict_types=1);

namespace App\Cost\Application\Manager\Discount;

use App\Cost\Infrastructure\Repository\DiscountByTravelDateRepository;

class PriceDiscount extends Discount
{
    private \DateTimeImmutable $dateOfTravel;
    private \DateTimeImmutable $dateOfPayment;

    private int $price;

    public function __construct(
        private readonly DiscountByTravelDateRepository $travelDateRepository,
    )
    {
    }

    public function getAmountDiscount(): int
    {
        return $this->discountByPayment($this->dateOfTravel, $this->dateOfPayment, $this->price);
    }

    public function to(\DateTimeImmutable $dateOfTravel, \DateTimeImmutable $dateOfPayment, int $price): static
    {
        return $this
            ->setDateOfTravel($dateOfTravel)
            ->setDateOfPayment($dateOfPayment)
            ->setPrice($price);
    }

    public function setDateOfPayment(\DateTimeImmutable $dateOfPayment): static
    {
        $this->dateOfPayment = $dateOfPayment;

        return $this;
    }

    public function setDateOfTravel(\DateTimeImmutable $dateOfTravel): static
    {
        $this->dateOfTravel = $dateOfTravel;

        return $this;
    }

    public function setPrice(int $price): static
    {
        $this->price = $price;

        return $this;
    }

    private function discountByPayment(\DateTimeImmutable $dateOfTravel, \DateTimeImmutable $dateOfPayment, int $price): int
    {
        $discountData = $this->travelDateRepository->findDiscountByDate($dateOfTravel, $dateOfPayment);

        return $this->calculateDiscount($discountData, $price);
    }
}