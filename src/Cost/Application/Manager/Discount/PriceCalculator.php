<?php
declare(strict_types=1);

namespace App\Cost\Application\Manager\Discount;

use App\Cost\Application\Contract\DiscountInterface;

class PriceCalculator
{
    public function __construct(
        private MethodCalculateOfDiscount $methodCalculateOfDiscount
    )
    {
    }

    private array $discount = [];
    public function setDiscount(DiscountInterface $sale): static
    {
        $this->discount[] = $sale;
        return $this;
    }

    public function calculate(int $price): int
    {

        return array_reduce($this->discount, $this->methodCalculateOfDiscount, $price);
    }
}