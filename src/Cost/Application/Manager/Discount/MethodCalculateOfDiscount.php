<?php
declare(strict_types=1);

namespace App\Cost\Application\Manager\Discount;

use App\Cost\Application\Contract\DiscountInterface;

class MethodCalculateOfDiscount
{
    public function __invoke(int $total, DiscountInterface $sale): int
    {
        return $total - $sale->getAmountDiscount();
    }
}