<?php
declare(strict_types=1);

namespace App\Cost\Application\Manager\Discount;

use App\Cost\Application\Contract\DiscountInterface;
use App\Cost\Application\Dto\DiscountFindResultDto;

abstract class Discount implements DiscountInterface
{
    const TOTAL_PERCENT = 100;

    abstract public function getAmountDiscount(): int;

    protected function calculateDiscount(DiscountFindResultDto $discountData, int $price): int
    {
        $draftSail = (int)($price * $this->percentIntoShares($discountData->getAmount()));

        if (!empty($discountData->getMaxDiscountLimit())) {
            return min($draftSail, $discountData->getMaxDiscountLimit());
        }
        return $draftSail;
    }

    protected function percentIntoShares(int $percent): float
    {
        return (float)$percent / static::TOTAL_PERCENT;
    }
}