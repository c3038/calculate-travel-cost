<?php
declare(strict_types=1);

namespace App\Cost\Application\Manager\Discount;

use App\Cost\Infrastructure\Repository\DiscountByAgeRepository;

class AgeDiscount extends Discount
{
    private \DateTimeImmutable $dateOfBirth;
    private int $price;

    public function __construct(
        private readonly DiscountByAgeRepository $ageRepository,
    )
    {
    }

    public function getAmountDiscount(): int
    {
        return $this->discountByAge($this->calculateAge($this->dateOfBirth), $this->price);
    }

    public function to(\DateTimeImmutable $dateOfBirth, int $price): static
    {
        return $this
            ->setDateOfBirth($dateOfBirth)
            ->setPrice($price);
    }

    public function setDateOfBirth(\DateTimeImmutable $dateOfBirth): static
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function setPrice(int $price): static
    {
        $this->price = $price;

        return $this;
    }


    private function calculateAge(\DateTimeImmutable $dateOfBirth): int
    {
        return (new \DateTimeImmutable())->diff($dateOfBirth)->y;
    }

    private function discountByAge(int $age, int $price): int
    {
        $discountData = $this->ageRepository->findByAge($age);

        return $this->calculateDiscount($discountData, $price);
    }
}