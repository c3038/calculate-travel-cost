<?php
declare(strict_types=1);

namespace App\Cost\Application\Mapper;

use App\Cost\Application\Dto\DiscountByAgeCreatedDto;
use App\Cost\Domain\Entity\DiscountByAge;

class DiscountByAgeMapper
{
    public function toCreatedDto(DiscountByAge $discountByAge): DiscountByAgeCreatedDto
    {
        return new DiscountByAgeCreatedDto(
            $discountByAge->getId(),
            $discountByAge->getAgeLimit()->getPrimary()->getValue(),
            $discountByAge->getAgeLimit()->getFinal()->getValue(),
            $discountByAge->getAmount()->getValue(),
            $discountByAge->getMaxDiscountLimit(),
            $discountByAge->getCreatedAt(),
            $discountByAge->getUpdatedAt(),
        );
    }
}