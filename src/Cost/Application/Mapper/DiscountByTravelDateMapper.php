<?php
declare(strict_types=1);

namespace App\Cost\Application\Mapper;

use App\Cost\Application\Dto\DiscountByTravelDateCreatedDto;
use App\Cost\Domain\Entity\DiscountByTravelDate;

class DiscountByTravelDateMapper
{
    public function toCreatedDto(DiscountByTravelDate $discountByTravelDate): DiscountByTravelDateCreatedDto
    {
        return new DiscountByTravelDateCreatedDto(
            $discountByTravelDate->getId(),
            $discountByTravelDate->getDataLimit()->getStartDate(),
            $discountByTravelDate->getDataLimit()->getEndDate(),
            $discountByTravelDate->getCreatedAt(),
            $discountByTravelDate->getUpdatedAt(),
        );
    }
}